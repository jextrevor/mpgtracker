package com.trevorjex.mpgtracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.trevorjex.mpgtracker.models.Fillup;

import java.math.RoundingMode;
import java.text.NumberFormat;

public class FillupsAdapter extends RecyclerView.Adapter<FillupsAdapter.ViewHolder> {
    ObservableArrayList<Fillup> fillups;

    public interface OnFillupClicked {
        public void onClick(Fillup fillup);
    }

    OnFillupClicked listener;

    public FillupsAdapter(ObservableArrayList<Fillup> fillups, OnFillupClicked listener) {
        this.fillups = fillups;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fillup_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Fillup fillup = fillups.get(position);
        TextView endMileage = holder.itemView.findViewById(R.id.endMileage);
        endMileage.setText("Odometer: " + String.valueOf(fillup.endMileage));
        TextView mileage = holder.itemView.findViewById(R.id.mileage);
        mileage.setText("Mileage: " + String.valueOf(fillup.endMileage - fillup.startMileage));

        double mpg = (fillup.endMileage - fillup.startMileage) / fillup.gallons;
        TextView mpgField = holder.itemView.findViewById(R.id.mpg);
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(1);
        nf.setRoundingMode(RoundingMode.HALF_UP);
        mpgField.setText(nf.format(mpg) + " mpg");
        holder.itemView.setOnClickListener(v -> {
            listener.onClick(fillups.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return fillups.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
