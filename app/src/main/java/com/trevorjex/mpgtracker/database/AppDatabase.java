package com.trevorjex.mpgtracker.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.trevorjex.mpgtracker.models.Fillup;

@Database(entities = {Fillup.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract FillupsDao getFillupsDao();
}
