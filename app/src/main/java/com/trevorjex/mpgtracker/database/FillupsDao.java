package com.trevorjex.mpgtracker.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.trevorjex.mpgtracker.models.Fillup;

import java.util.List;

@Dao
public interface FillupsDao {
    @Insert
    public long insert(Fillup fillup);

    @Query("SELECT * FROM fillup ORDER BY end_mileage DESC")
    public List<Fillup> getAll();

    @Query("SELECT * FROM fillup WHERE id = :id LIMIT 1")
    public Fillup getByID(long id);

    @Query("SELECT * FROM fillup ORDER BY end_mileage DESC LIMIT 1")
    public Fillup getLastFillup();

    @Delete
    public void delete(Fillup fillup);

    @Update
    public void update(Fillup fillup);
}
