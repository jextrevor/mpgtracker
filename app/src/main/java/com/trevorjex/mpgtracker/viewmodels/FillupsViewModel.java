package com.trevorjex.mpgtracker.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.trevorjex.mpgtracker.database.AppDatabase;
import com.trevorjex.mpgtracker.models.Fillup;

import java.util.ArrayList;

public class FillupsViewModel extends AndroidViewModel {
    private final AppDatabase database;
    MutableLiveData<Boolean> saving = new MutableLiveData<>();
    MutableLiveData<Boolean> deleting = new MutableLiveData<>();
    MutableLiveData<Integer> latestMileage = new MutableLiveData<>();
    ObservableArrayList<Fillup> fillups = new ObservableArrayList<>();
    MutableLiveData<Fillup> editingFillup = new MutableLiveData<>();

    public FillupsViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "fillupsdb").build();
        new Thread(() -> {
            updateLatestMileage();

            ArrayList<Fillup> fillupsArrayList = (ArrayList<Fillup>) database.getFillupsDao().getAll();
            fillups.addAll(fillupsArrayList);
        }).start();
    }

    public void saveFillup(int startMileage, int endMileage, double gallons) {
        if (latestMileage.getValue() == null || endMileage > latestMileage.getValue()) {
            latestMileage.setValue(endMileage);
        }
        saving.setValue(true);
        new Thread(() -> {
            Fillup fillup = new Fillup();
            fillup.startMileage = startMileage;
            fillup.endMileage = endMileage;
            fillup.gallons = gallons;
            fillup.id = database.getFillupsDao().insert(fillup);

            fillups.add(fillup);
            fillups.sort((a, b) -> {
                return b.endMileage - a.endMileage;
            });

            saving.postValue(false);
        }).start();
    }

    public void updateFillup(Fillup fillup) {
        saving.setValue(true);
        new Thread(() -> {
            database.getFillupsDao().update(fillup);
            updateLatestMileage();

            fillups.removeIf(f -> f.id == fillup.id);
            fillups.add(fillup);
            fillups.sort((a, b) -> {
                return b.endMileage - a.endMileage;
            });

            saving.postValue(false);
        }).start();
    }

    public void deleteFillup(Fillup fillup) {
        deleting.setValue(true);
        new Thread(() -> {
            database.getFillupsDao().delete(fillup);
            updateLatestMileage();
            fillups.removeIf(f -> f.id == fillup.id);
            deleting.postValue(false);
        }).start();
    }

    public MutableLiveData<Integer> getLatestMileage() {
        return latestMileage;
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public MutableLiveData<Boolean> getDeleting() {
        return deleting;
    }

    public ObservableArrayList<Fillup> getFillups() {
        return fillups;
    }

    public MutableLiveData<Fillup> getEditingFillup() {
        return editingFillup;
    }

    public void setEditingFillup(Fillup fillup) {
        this.editingFillup.setValue(fillup);
    }

    private void updateLatestMileage() {
        Fillup lastFillup = database.getFillupsDao().getLastFillup();
        if (lastFillup != null) {
            latestMileage.postValue(lastFillup.endMileage);
        } else {
            latestMileage.postValue(null);
        }
    }
}
