package com.trevorjex.mpgtracker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.trevorjex.mpgtracker.fragments.FillupListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_view, FillupListFragment.class, null).setReorderingAllowed(true).commit();
        }
    }
}