package com.trevorjex.mpgtracker.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Fillup {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name="start_mileage")
    public int startMileage;
    @ColumnInfo(name="end_mileage")
    public int endMileage;
    @ColumnInfo
    public double gallons;
}
