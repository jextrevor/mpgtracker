package com.trevorjex.mpgtracker.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.trevorjex.mpgtracker.R;
import com.trevorjex.mpgtracker.models.Fillup;
import com.trevorjex.mpgtracker.viewmodels.FillupsViewModel;

public class AddEditFillupFragment extends Fragment {
    public AddEditFillupFragment() {
        super(R.layout.fragment_add_edit_fillup);
    }

    private boolean wasSaving = false;
    private boolean wasDeleting = false;
    private Fillup currentFillup = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FillupsViewModel viewmodel = new ViewModelProvider(getActivity()).get(FillupsViewModel.class);
        viewmodel.getLatestMileage().observe(getViewLifecycleOwner(), (latestMileage) -> {
            if (currentFillup == null) {
                View viewToFocus;
                EditText startMileageField = view.findViewById(R.id.start_mileage_field);
                if (latestMileage == null) {
                    startMileageField.setText("");
                    viewToFocus = startMileageField;
                } else {
                    startMileageField.setText(String.valueOf(latestMileage));
                    viewToFocus = view.findViewById(R.id.end_mileage_field);
                }
                viewToFocus.requestFocus();
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(viewToFocus, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        viewmodel.getEditingFillup().observe(getViewLifecycleOwner(), fillup -> {
            currentFillup = fillup;
            if (fillup != null) {
                EditText startMileageField = view.findViewById(R.id.start_mileage_field);
                startMileageField.setText(String.valueOf(fillup.startMileage));
                EditText endMileageField = view.findViewById(R.id.end_mileage_field);
                endMileageField.setText(String.valueOf(fillup.endMileage));
                EditText gallonsField = view.findViewById(R.id.gallons_field);
                gallonsField.setText(String.valueOf(fillup.gallons));
                Button deleteButton = view.findViewById(R.id.delete_button);
                deleteButton.setEnabled(true);
            } else {
                Button deleteButton = view.findViewById(R.id.delete_button);
                deleteButton.setEnabled(false);
            }
        });

        viewmodel.getSaving().observe(getViewLifecycleOwner(), (saving) -> {
            if (!wasSaving && saving) {
                Button saveButton = view.findViewById(R.id.save_button);
                saveButton.setEnabled(false);
                saveButton.setText("Saving...");
                wasSaving = saving;
            } else if (wasSaving && !saving) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        viewmodel.getDeleting().observe(getViewLifecycleOwner(), (deleting) -> {
            if (!wasDeleting && deleting) {
                Button deleteButton = view.findViewById(R.id.delete_button);
                deleteButton.setEnabled(false);
                deleteButton.setText("Deleting...");
                wasDeleting = deleting;
            } else if (wasDeleting && !deleting) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        view.findViewById(R.id.save_button).setOnClickListener(v -> {
            saveFillup();
        });
        view.findViewById(R.id.delete_button).setOnClickListener(v -> {
            deleteFillup();
        });

        ((EditText) view.findViewById(R.id.gallons_field)).setOnEditorActionListener((textView, i, keyEvent) -> {
            if ((i & EditorInfo.IME_MASK_ACTION) == EditorInfo.IME_ACTION_DONE) {
                saveFillup();
                return true;
            }
            return false;
        });
    }

    private void saveFillup() {
        FillupsViewModel viewmodel = new ViewModelProvider(getActivity()).get(FillupsViewModel.class);
        EditText startMileageField = getView().findViewById(R.id.start_mileage_field);
        EditText endMileageField = getView().findViewById(R.id.end_mileage_field);
        EditText gallonsField = getView().findViewById(R.id.gallons_field);
        int startMileage, endMileage;
        double gallons;
        try {
            startMileage = Integer.parseInt(startMileageField.getText().toString());
        } catch (NumberFormatException e) {
            startMileageField.setError("Start mileage is required");
            return;
        }
        try {
            endMileage = Integer.parseInt(endMileageField.getText().toString());
        } catch (NumberFormatException e) {
            endMileageField.setError("End mileage is required");
            return;
        }
        try {
            gallons = Double.parseDouble(gallonsField.getText().toString());
        } catch (NumberFormatException e) {
            gallonsField.setError("Gallons is required");
            return;
        }

        if (currentFillup == null) {

            viewmodel.saveFillup(startMileage, endMileage, gallons);
        } else {
            currentFillup.startMileage = startMileage;
            currentFillup.endMileage = endMileage;
            currentFillup.gallons = gallons;
            viewmodel.updateFillup(currentFillup);
        }
    }

    private void deleteFillup() {
        FillupsViewModel viewmodel = new ViewModelProvider(getActivity()).get(FillupsViewModel.class);
        if (currentFillup != null) {
            viewmodel.deleteFillup(currentFillup);
        }
    }
}
