package com.trevorjex.mpgtracker.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.trevorjex.mpgtracker.FillupsAdapter;
import com.trevorjex.mpgtracker.R;
import com.trevorjex.mpgtracker.fragments.AddEditFillupFragment;
import com.trevorjex.mpgtracker.viewmodels.FillupsViewModel;

public class FillupListFragment extends Fragment {
    public FillupListFragment() {
        super(R.layout.fragment_fillup_list);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FillupsViewModel viewmodel = new ViewModelProvider(getActivity()).get(FillupsViewModel.class);
        ObservableArrayList fillups = viewmodel.getFillups();
        FillupsAdapter adapter = new FillupsAdapter(fillups, fillup -> {
            viewmodel.setEditingFillup(fillup);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_view, AddEditFillupFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
        fillups.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    adapter.notifyDataSetChanged();
                });

            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    adapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    adapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    adapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    adapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_fillups);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.new_button).setOnClickListener(v -> {
            viewmodel.setEditingFillup(null);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_view, AddEditFillupFragment.class, null).setReorderingAllowed(true).addToBackStack(null).commit();
        });
    }
}
